import React from 'react';
import { Link } from 'react-router';
import Classes from 'styles/core/header/header.css';

const links = [
  { to: '/', children: 'Home' },
  { to: '/dashboard', children: 'Dashboard' }
];

const Header = () => (
  <div className={ Classes.root }>
    { links.map((props, i) =>
      <Link
        key={ i }
        activeClassName={ Classes.active }
        activeOnlyWhenExact
        { ...props }
      />
    )}
    <div>
      <span>Icon</span>
      <span className="fa fa-hand-spock-o fa-1g">Oh yeah</span>
      <button className="pure-button pure-button-disabled">A Disabled Button</button>
      <button className="pure-button" disabled>A Disabled Button</button>
      <div className="fa fa-hand-spock-o fa-1g">
        asdasss
      </div>
    </div>
  </div>
);

export default Header;

