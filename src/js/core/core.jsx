import React from 'react';
import { BrowserRouter, Match } from 'react-router';
import { Provider } from 'react-redux';
import Dashboard from 'js/dashboard';
import Home from 'js/home';
import Header from 'js/core/header';
import Classes from 'styles/core/core.css';

const Core = ({ store }) => (
  <Provider store={ store }>
    <BrowserRouter>
      <span className={ Classes.root }>
        <Header />

        <Match exactly pattern="/" component={ Home } />
        <Match exactly pattern="/dashboard" component={ Dashboard } />
      </span>
    </BrowserRouter>
  </Provider>
);

Core.propTypes = {
  store:    Provider.propTypes.store // eslint-disable-line
};

export default Core;
