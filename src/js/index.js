import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import store from 'js/core/config/create-store';
import Core from 'js/core';
// TODO: TECH DEBT
// This should be reviewed. We can't import it through css-loader with css modules due to a bug
import 'styles/globals.gcss';


const render = () => {
  ReactDOM.render(
    <AppContainer>
      <Core store={ store } />
    </AppContainer>,
    document.getElementById('root')
  );
};

render(Core);

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('js/core', () => {
    render(Core);
  });
}
