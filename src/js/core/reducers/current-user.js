import createReducer from 'js/helpers/create-reducer';
import { Map } from 'immutable';

const getDefaultState = () => (
  Map({
    data: Map()
  })
);

const currentUser = createReducer(getDefaultState(), {});


export default currentUser;
