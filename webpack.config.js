const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const merge = require('webpack-merge');
const glob = require('glob');
const fs = require('fs');
const pkg = require('./package.json');

const parts = require('./webpack/webpack.parts');

const BASE_DIR = __dirname;
const PATHS = {
  app: path.join(BASE_DIR, 'src/js'),
  build: path.join(BASE_DIR, 'build'),
  assets: path.join(BASE_DIR, 'src/assets'),
  styles: path.join(BASE_DIR, 'src/styles'),
  license: path.join(BASE_DIR, 'LICENSE'),
  src: path.join(BASE_DIR, 'src')
};

const common = merge([
  {
    output: {
      path: PATHS.build,
      publicPath: '/',
      filename: '[name].js',
    },
  },
  parts.lintJavaScript({ include: PATHS.app }),
  parts.lintCSS({include: PATHS.styles}),
  parts.loadFonts({
    options: {
      name: 'assets/[folder]/[name].[hash:8].[ext]',
    },
  }),
  /*parts.ignore({
    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
    include: /font-awesome/,
  }),*/
  parts.loadJavaScript({include: PATHS.app}),
  parts.setAlias({
    js: path.resolve(PATHS.app),
    styles: path.resolve(PATHS.styles),
    assets: path.resolve(PATHS.assets)
  })
]);

function production() {
  return merge([
    common,
    {
      performance: {
        hints: 'warning', // 'error' or false are valid too
        maxEntrypointSize: 100000, // in bytes
        maxAssetSize: 200000, // in bytes
      },
      output: {
        chunkFilename: '[name].[chunkhash:8].js',
        filename: '[name].[chunkhash:8].js',
      },
      plugins: [
        new webpack.HashedModuleIdsPlugin(),
      ],
      recordsPath: 'records.json',
    },
    parts.minifyJavaScript({useSourceMap: true}),
    parts.minifyCSS({
      options: {
        discardComments: {
          removeAll: true,
          // Run cssnano in safe mode to avoid
          // potentially unsafe ones.
          safe: true,
        },
      },
    }),
    parts.attachBannerText({
      versionText: pkg.version + '-' + (process.env.BUILD_NUMBER || 1),
      licenseText: fs.readFileSync(PATHS.license, 'utf8')
    }),
    parts.extractBundles([
      {
        name: 'spa',
        chunks: ['app'],
        minChunks: ({userRequest}) => (
          userRequest &&
          userRequest.indexOf('node_modules') >= 0 &&
          new RegExp(['react/', 'react-dom', 'react-router'].join("|")).test(userRequest)   &&
          userRequest.match(/\.js$/)
        )
      },
      {
        name: 'vendor',
        chunks: ['app'],
        minChunks: ({ userRequest }) => (
          userRequest &&
          userRequest.indexOf('node_modules') >= 0 &&
          userRequest.match(/\.js$/)
        )
      },
      {
        name: 'manifest',
        minChunks: Infinity,
      },
    ]),
    parts.generateSourceMaps({type: 'source-map'}),
    parts.extractCSS({}),
    /*parts.purifyCSS({
      paths: glob.sync(path.join(PATHS.styles, '**', '*')),
    }),*/
    parts.loadImages({
      include: PATHS.assets,
      options: {
        limit: 1000,
        name: 'assets/[folder]/[name].[hash:8].[ext]',
      },
    }),
    parts.setFreeVariable(
      'process.env.NODE_ENV',
      'production'
    ),
  ]);
}

function development() {
  return merge([
    common,
    {
      output: {
        devtoolModuleFilenameTemplate: 'webpack:///[absolute-resource-path]',
      },
      plugins: [
        new webpack.NamedModulesPlugin(),
      ],
    },
    parts.generateSourceMaps({type: 'eval-source-map'}),
    parts.devServer({
      // Customize host/port here if needed
      host: process.env.HOST,
      port: process.env.PORT,
    }),
    parts.loadCSS(),
    parts.loadImages({
      include: PATHS.assets,
      options: {
        limit: 1000,
      },
    }),
  ]);
}

function app() {
  return {
    entry: {
      app: PATHS.app,
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: PATHS.src + '/index.ejs',
        title: 'EqualsMagic',
        filename: 'index.html',
        inject: 'body', // html-webpack-template needs this to work
        chunksSortMode: (chunk1, chunk2) => {
          const orders = ['manifest', 'spa', 'vendor', 'app'];
          const order1 = orders.indexOf(chunk1.names[0]);
          const order2 = orders.indexOf(chunk2.names[0]);
          return order1 > order2 ? 1 : -1;
        }
      }),
    ],
  };
}

function appDevelopment() {
  return {
    entry: {
      // react-hot-loader has to run before demo!
      app: ['react-hot-loader/patch', PATHS.app],
    },
  };
}

module.exports = function (env) {
  process.env.BABEL_ENV = env;

  if (env === 'production') {
    return merge(production(), app());
  }

  return merge(development(), app(), appDevelopment());
};
