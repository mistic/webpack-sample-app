// Make Enzyme functions available in all test files without importing
import React from 'react';
import { shallow, render, mount } from 'enzyme';
import renderer from 'react-test-renderer';

global.shallow = shallow;
global.render = render;
global.mount = mount;
global.React = React;
global.reactTestRenderer = renderer;

// Skip createElement warnings but fail tests on any other warning
console.error = message => {
  if (!/(React.createElement: type should not be null)/.test(message)) {
    throw new Error(message);
  }
};

// It's possible to use Enzyme shallow with jest snapshot testing, we have to install enzyme-to-json npm package
// and add this to package.json inside jest config
//
/*
"snapshotSerializers": [
  "<rootDir>/node_modules/enzyme-to-json/serializer"
],
*/
//
